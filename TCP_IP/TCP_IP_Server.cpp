﻿// TCP_IP.cpp : このファイルには 'main' 関数が含まれています。プログラム実行の開始と終了がそこで行われます。
//
// プログラムの実行: Ctrl + F5 または [デバッグ] > [デバッグなしで開始] メニュー
// プログラムのデバッグ: F5 または [デバッグ] > [デバッグの開始] メニュー

// 作業を開始するためのヒント: 
//    1. ソリューション エクスプローラー ウィンドウを使用してファイルを追加/管理します 
//   2. チーム エクスプローラー ウィンドウを使用してソース管理に接続します
//   3. 出力ウィンドウを使用して、ビルド出力とその他のメッセージを表示します
//   4. エラー一覧ウィンドウを使用してエラーを表示します
//   5. [プロジェクト] > [新しい項目の追加] と移動して新しいコード ファイルを作成するか、[プロジェクト] > [既存の項目の追加] と移動して既存のコード ファイルをプロジェクトに追加します
//   6. 後ほどこのプロジェクトを再び開く場合、[ファイル] > [開く] > [プロジェクト] と移動して .sln ファイルを選択します

#include <iostream>
#include <stdio.h>
#include <winsock2.h>
#include <ws2tcpip.h>

#include "main.h"

char server_ip[128] = "192.168.1.151";//"127.0.0.1";

extern FILE* fp;

int server(char *strs)
{
	SOCKET sockfd; //int sockfd;
	SOCKET client_sockfd; //int client_sockfd;
	struct sockaddr_in addr;
	socklen_t len = sizeof(struct sockaddr_in);
	struct sockaddr_in from_addr;

	char buf[4096];
	char send_buf[4096];

	// 受信バッファ初期化
	memset(buf, 0, sizeof(buf));


	// ソケット生成
	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		perror("socket");
	}

	// 待ち受け用IP・ポート番号設定
	addr.sin_family = AF_INET;
	addr.sin_port = htons(8501); //htons() 関数は、短整数をホスト・バイト・オーダーからネットワーク・バイト・オーダーに変換します。

	if (strs == NULL)
	{
		addr.sin_addr.s_addr = INADDR_ANY; //INADDR_ANY;
	}
	else
	{
		printf("%s %d %s \n", __func__, __LINE__, strs);
		inet_pton(AF_INET, strs, &addr.sin_addr.s_addr); //addr.sin_addr.s_addr = inet_addr(strs);
	}
	// バインド
	if (bind(sockfd, (struct sockaddr*)&addr, sizeof(addr)) < 0) {
		perror("bind");
	}
	printf("%s %d Server IP=%d.%d.%d.%d port=%d\n", __func__, __LINE__, (addr.sin_addr.s_addr & 0xff),
		((addr.sin_addr.s_addr & 0xff00) >> 8),
		((addr.sin_addr.s_addr & 0xff0000) >> 16),
		((addr.sin_addr.s_addr & 0xff000000) >> 24),
		ntohs(addr.sin_port)
	);
	// 受信待ち
	printf("%s %d wait listen\n", __func__, __LINE__);
	if (listen(sockfd, SOMAXCONN) < 0) {
		perror("listen");
	}

	// クライアントからのコネクト要求待ち
	printf("%s %d wait client connect\n", __func__, __LINE__);
	if ((client_sockfd = accept(sockfd, (struct sockaddr*)&from_addr, &len)) < 0) {
		perror("accept");
	}
	printf("%s %d connected Crient ", __func__, __LINE__);
	printf("PORT=%d ", ntohs(from_addr.sin_port));
	printf("IP=%d.%d.%d.%d\n",
		(from_addr.sin_addr.s_addr & 0xff),
		((from_addr.sin_addr.s_addr & 0xff00) >> 8),
		((from_addr.sin_addr.s_addr & 0xff0000) >> 16),
		((from_addr.sin_addr.s_addr & 0xff000000) >> 24)
	);
	// 受信
	int rsize;

	time_t now;
	struct tm local = {};
	time_t start_time = time(NULL);

	int file_state = FILE_OPEN;
	int file_seq_no = 1;
	int diff_time[2] = { 0,-1 };



	while (1) {
		memset((char*)&buf, 0x0, sizeof(buf));
		rsize = recv(client_sockfd, buf, sizeof(buf), 0);
		printf("%s %d %s rsize=%d \n", __func__, __LINE__, buf, rsize);

		diff_time[0] = (int)difftime(time(NULL), start_time);
		if (diff_time[0] == 0)
		{
			// 60秒*60分=1時間経過で 余り 0 になる
		}
		//if ((diff_time % (60*60))==0)
		else if ((diff_time[0] % (60 * 60)) == 0)
		{
			//printf("%s %d file_state=%d diff_time[0]=%d [1]=%d\n",__func__,__LINE__,file_state,diff_time[0],diff_time[1]);
			if (diff_time[0] == diff_time[1])
			{
			}
			else
			{
				diff_time[1] = diff_time[0];
				if (file_state == FILE_WRITE)
				{
					file_state = FILE_CLOSE;
				}
			}

		}
		if (file_state == FILE_CLOSE)
		{
			fclose(fp);
			file_state = FILE_OPEN;
		}
		if (file_state == FILE_OPEN)
		{
			fp = file_open(file_seq_no);
			file_seq_no++;
			file_state = FILE_WRITE;
		}



		if (rsize == 0) {
			//接続先が正しくシャットダウンを実行した場合は、返り値は 0 となる。
			printf("%s %d %d client shotdown\n", __func__, __LINE__, rsize);
			break;
		}
		else if (rsize == -1) {
			printf("%s %d\n", __func__, __LINE__);
			perror("recv error");
		}
		else {
			// 時間表示
			now = time(NULL); //現在時刻の取得
			errno_t err = localtime_s(&local,&now);

			char str[1020];
			sprintf_s(str, "%04d/%02d/%02d %02d:%02d:%2d %08d"
				, local.tm_year + 1900
				, local.tm_mon + 1
				, local.tm_mday
				, local.tm_hour
				, local.tm_min
				, local.tm_sec
				, (int)diff_time[0]
			);

			//printf( "%s %d %s %s\n",__func__,__LINE__,str, buf );

			fprintf(fp, "%s %s\n", str, buf);
			//sleep( 1 );
/*********************************************************************************
*
*  ここに受信したコマンドを PLCコマンド変換し mySql DBにアクセスする処理を書く
*
**********************************************************************************/
			memset((char*)&send_buf, 0x0, sizeof(send_buf));
			//plcCmd(buf, send_buf);
			if (strncmp(buf, "START", 5) == 0)
			{
				sprintf_s(send_buf, "%s", "OK");  // 応答 OK
				printf("%s %d %s%s\n", __func__, __LINE__, buf, send_buf);
			}
			else
			{
				// mySql が面倒くさいので使わないバージョンにした 2020/6/21
#if 0
				plcCmd(buf, send_buf);
#else
				sprintf_s(send_buf, "%s", "OK"); // 応答 OK
#endif
			}
			//sprintf(send_buf,"%s","OK");  // 応答 OK
			//int ssize = 2;
			size_t ssize = strlen(send_buf);
			//printf( "%s %d send(size=%d):%s\n",__func__,__LINE__, ssize,send_buf );
			send(client_sockfd, send_buf, (int)ssize, 0);
			//write(client_sockfd, send_buf, ssize);
}
	}
	printf("%s %d\n", __func__, __LINE__);

	// ソケットクローズ
	closesocket(client_sockfd);
	closesocket(sockfd);
	//close(client_sockfd);
	//close(sockfd);
	fclose(fp);

#if 0
	int i;
	// ポート番号，ソケット
	int srcSocket;  // 自分
	int dstSocket;  // 相手

	// sockaddr_in 構造体
	struct sockaddr_in srcAddr;
	struct sockaddr_in dstAddr;
	int dstAddrSize = sizeof(dstAddr);
	int status;
	// 各種パラメータ
	int numrcv;
	char buffer[1024];

	// Windows の場合
	//WSADATA data;
	//WSAStartup(MAKEWORD(2, 0), &data);
	// sockaddr_in 構造体のセット
	memset(&srcAddr, 0, sizeof(srcAddr));
	srcAddr.sin_port = htons(PORT);
	srcAddr.sin_family = AF_INET;
	srcAddr.sin_addr.s_addr = htonl(INADDR_ANY);

	// ソケットの生成（ストリーム型）
	srcSocket = socket(AF_INET, SOCK_STREAM, 0);
	// ソケットのバインド
	bind(srcSocket, (struct sockaddr*)&srcAddr, sizeof(srcAddr));
	// 接続の許可
	listen(srcSocket, 1);

	while (1)
	{ //ループで回すことによって何度でもクライアントからつなぐことができる

		// 接続の受付け
		printf("接続を待っています\nクライアントプログラムを動かしてください\n");
		dstSocket = accept(srcSocket, (struct sockaddr*)&dstAddr, &dstAddrSize);
		//printf("%s から接続を受けました\n", inet_ntoa(dstAddr.sin_addr));

		while (1) {
			//パケットの受信
			numrcv = recv(dstSocket, buffer, sizeof(char) * 1024, 0);
			if (numrcv == 0 || numrcv == -1) {
				status = closesocket(dstSocket); break;
			}
			printf("変換前 %s", buffer);
			for (i = 0; i < numrcv; i++) { // bufの中の小文字を大文字に変換
				//if(isalpha(buffer[i])) 
				buffer[i] = toupper(buffer[i]);
			}
			// パケットの送信
			send(dstSocket, buffer, sizeof(char) * 1024, 0);
			printf("→ 変換後 %s \n", buffer);
		}
	}
#endif


	return(0);
}

