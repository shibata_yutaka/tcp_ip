#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <winsock2.h>
#include <ws2tcpip.h>

#include "main.h"


//int sockfd;
SOCKET sockfd;
struct sockaddr_in addr;

char send_str[1024];    // データ送信
char receive_str[1024];
int server_port = 8501;
//char server_ip[128] = //"127.0.0.1";
extern char server_ip[];
/*
*
*/
typedef struct
{
    char kbn[3];
    int  no;
    char sign[3];
} device_t;

typedef struct
{
    char cmd[4];
    device_t device;
    int num;
} cmd_table_t;

cmd_table_t cmdTable[9] =
{
    {"WRS","DM", 8540,".U", 4},
    {"WRS","DM",46000,".S", 40},
    {"WRS","DM",47000,".S", 40},
    {"WRS","DM",48000,".S", 40},
    {"WRS","DM",49000,".S", 30},
    {"WRS","DM",46150,".S", 4},
    {"WRS","DM",47150,".S", 4},
    {"WRS","DM",48150,".S", 4},
    {"WRS","DM",49150,".S", 3}
};

void socketInit(void);


int client(char *strs) 
{
	socketInit();
	printf("%s %d\n",__func__,__LINE__);
	time_t now;
    struct tm local = {};

    int heartBeat[4]={0,1,2,3};
    size_t size = 0;//int size=0;
    while (1)
    {
	    now = time(NULL); //現在時刻の取得
        errno_t err = localtime_s(&local,&now);

        //sprintf( send_str, "RDS DM%05d.S 3\r",46000 );
        int tableMax = sizeof(cmdTable) / sizeof(cmdTable[0]);
        //printf("%s %d %d %d %d\n",__func__,__LINE__,(int)sizeof(cmdTable),(int)sizeof(cmdTable[0]),tableMax);
        for (int i=0; i<tableMax; i++)
        {
            switch (i)
            {
                case 0:
            		memset(send_str,0x00,sizeof(send_str));
                    sprintf_s(send_str,"%s %s%05d%s %d"
                        ,cmdTable[i].cmd
                        ,cmdTable[i].device.kbn
                        ,cmdTable[i].device.no
                        ,cmdTable[i].device.sign
                        ,cmdTable[i].num
                        );
                    size = strlen(send_str);
                    //printf("%s %d %d\n",__func__,__LINE__,size);
                    for (int j=0; j<cmdTable[i].num; j++)
                    {
            			char tmp[10];
                        sprintf_s(tmp," %d",heartBeat[j]);
                    	strcat_s(send_str,tmp);
                    }
            		strcat_s(send_str,"\r");
                    break;
                default:
            		memset(send_str,0x00,sizeof(send_str));
                    sprintf_s(send_str,"%s %s%05d%s %d"
                        ,cmdTable[i].cmd
                        ,cmdTable[i].device.kbn
                        ,cmdTable[i].device.no
                        ,cmdTable[i].device.sign
                        ,cmdTable[i].num
                        );
                    size = strlen(send_str);
                    //printf("%s %d %d\n",__func__,__LINE__,size);
                    int value = local.tm_sec;
                    for (int j=0; j<cmdTable[i].num; j++)
                    {
            			char tmp[10];
                        sprintf_s(tmp," %d",value+j);
                    	strcat_s(send_str,tmp);
                    }
            		strcat_s(send_str,"\r");
                    break;
            }
            printf( "%s %d %s\n",__func__,__LINE__,send_str);
            char temp[50];
            memset(temp,0x00,sizeof(temp));
            strncpy_s(temp,send_str,40);
            //printf( "%s %d %s\n",__func__,__LINE__,temp);


            if( send( sockfd, send_str, sizeof(send_str), 0 ) < 0 ) {
                printf_s("%s %d send error sockfd=%d send_str=%s\n",__func__,__LINE__, (int)sockfd ,&send_str);
                perror( "send" );
                return 0;
            }
            else
            {
                memset((char *)&receive_str,0x0,sizeof(receive_str));
                recv( sockfd, receive_str, sizeof(receive_str), 0 );
                printf( "%s %d %s\n",__func__,__LINE__, receive_str );
            }
        	//Sleep( 200 );

        }
        for (int i=0;i<4; i++)
        {
            heartBeat[i]++;
            if (heartBeat[i]>3)
            {
                heartBeat[i]=0;
            }
        }
        Sleep( 1000 );
    }

	printf("%s %d\n",__func__,__LINE__);

#if 0
	// IP アドレス，ポート番号，ソケット，sockaddr_in 構造体
	char Destination[32]="192.168.1.140";
	int dstSocket;
	struct sockaddr_in dstAddr;

	// 各種パラメータ
	char buffer[1024];


	// 相手先アドレスの入力と送る文字の入力
	//printf("サーバーマシンのIPは？:");
	//scanf("%s", destination);
	//strcpy(Destination, "192.168.1.140");

	// sockaddr_in 構造体のセット
	memset(&dstAddr, 0, sizeof(dstAddr));
	dstAddr.sin_port = htons(PORT);
	dstAddr.sin_family = AF_INET;
	
	if (strs == NULL)
	{
		dstAddr.sin_addr.s_addr = INADDR_ANY; //INADDR_ANY;
	}
	else
	{
		printf("%s %d %s \n", __func__, __LINE__, strs);
		inet_pton(AF_INET,strs,&dstAddr.sin_addr.s_addr); //inet_addr(Destination);
	}

	// ソケットの生成
	dstSocket = socket(AF_INET, SOCK_STREAM, 0);

	//接続
	if (connect(dstSocket, (struct sockaddr*)&dstAddr, sizeof(dstAddr))) {
		printf("%s　に接続できませんでした\n", Destination);
		return(-1);
	}
	printf("%s に接続しました\n", Destination);
	printf("適当なアルファベットを入力してください\n");

	while (1) {
		//scanf("%s", buffer);
		//パケットの送信
		send(dstSocket, buffer, 1024, 0);
		//パケットの受信
		recv(dstSocket, buffer, 1024, 0);
		printf("→ %s\n\n", buffer);
	}

	// Windows でのソケットの終了
	closesocket(dstSocket);
	//WSACleanup();
#endif
	return(0);
}


/*
*
*/
void socketInit(void)
{
	printf("%s %d\n", __func__, __LINE__);

	// ソケット生成
	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
	{
		printf("%s %d client socket make error\n", __func__, __LINE__);
		perror("socket");
		return;
	}

	// 送信先アドレス・ポート番号設定
	addr.sin_family = AF_INET;
	inet_pton(AF_INET, server_ip, &addr.sin_addr.s_addr);  //addr.sin_addr.s_addr = inet_addr(server_ip);
	addr.sin_port = htons(server_port);

	// サーバ接続
	connect(sockfd, (struct sockaddr*)&addr, sizeof(struct sockaddr_in));
	printf("PORT=%d ", ntohs(addr.sin_port));
	printf("IP=%d.%d.%d.%d\n",
		(addr.sin_addr.s_addr & 0xff),
		((addr.sin_addr.s_addr & 0xff00) >> 8),
		((addr.sin_addr.s_addr & 0xff0000) >> 16),
		((addr.sin_addr.s_addr & 0xff000000) >> 24)
	);

	printf("%s %d connect\n", __func__, __LINE__);
}
