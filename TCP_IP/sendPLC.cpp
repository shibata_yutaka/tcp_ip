
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <sys/types.h>

#include <iostream>

#include <winsock2.h>
#include <ws2tcpip.h>

#include "main.h"

extern SOCKET sockfd;

//char send_str[1024];    // データ送信
extern char receive_str[1024];
extern int server_port;
extern char server_ip[128];

int cmdR(ARGV_STRUCT_st argV)
{
    char buf[1024];
    char* strp = argV.strP;
    char* ctx  = argV.ctx;


    //printf("%s %d strS=%s strP=%s\n", __func__, __LINE__, argV.strS,argV.strP);
    strp=strtok_s(NULL, " \n\r", &ctx);
    if (strp == NULL)
    {
        sprintf_s(buf, "RDS DM%05d.S %d\r",8540, 4);
        printf("%s %d buf=%s\n", __func__, __LINE__,buf);
    }
    else
    {
        int adr = atoi(strp);
        //printf("%s %d strp=%s adr=%d\n", __func__, __LINE__,strp,adr);
        strp = (char*)strtok_s(NULL, " \n\r",&ctx);
        if (strp == NULL)
        {
            // 1 WORD READ
            sprintf_s(buf, "RD DM%05d.S\r", adr);
            printf("%s %d buf=%s\n", __func__, __LINE__, buf);

        }
        else
        {
            int num = atoi(strp);
            // 複数
            sprintf_s(buf, "RDS DM%05d.S %d\r", adr,num);
            printf("%s %d buf=%s\n", __func__, __LINE__, buf);
        }
    }
    // ソケット通信開始
    socketInit();
    /*
    * PLC に コマンドを送信する
    *   RDS DM08540.U 04
    */
    printf("%s %d %s\n", __func__, __LINE__, buf);

    if (send(sockfd, buf, sizeof(buf), 0) < 0) {
        printf("%s %d send error sockfd=%d send_str=%s\n", __func__, __LINE__, (int)sockfd, buf);
        perror("send");
        return 0;
    }
    /*
    *  PLCから RDSコマンドの返信を受け取る
    *  +00001 +00002 +00003 +00004
    */
    recv(sockfd, receive_str, sizeof(receive_str), 0);
    printf("%s %d %s\n", __func__, __LINE__, receive_str);

    // ソケットクローズ
    closesocket(sockfd);
    //printf("%s %d\n", __func__, __LINE__);
    return 0;

}
int cmdW(ARGV_STRUCT_st argV)
{
    char buf[1024];
    char* strp = argV.strP;
    char* ctx = argV.ctx;


    printf("%s %d strS=%s strP=%s\n", __func__, __LINE__, argV.strS,argV.strP);
    strp = strtok_s(NULL, " \n\r", &ctx);
    if (strp == NULL)
    {
        sprintf_s(buf, "WRS DM8540.S %d %d %d %d %d\r", 4,1,2,3,4);
        printf("%s %d buf=%s\n", __func__, __LINE__, buf);
    }
    else
    {
        int adr = atoi(strp);
        //printf("%s %d strp=%s adr=%d\n", __func__, __LINE__,strp,adr);
        strp = (char*)strtok_s(NULL, " \n\r", &ctx);
        if (strp == NULL)
        {
            // 1 WORD WRITE
            sprintf_s(buf, "WR DM%d.S\r", adr);
            printf("%s %d buf=%s\n", __func__, __LINE__, buf);

        }
        else
        {
            int dat = atoi(strp);
            // データ
            sprintf_s(buf, "WR DM%d.S %d\r", adr, dat);
            printf("%s %d buf=%s\n", __func__, __LINE__, buf);
        }
    }
    // ソケット通信開始
    socketInit();
    /*
    * PLC に コマンドを送信する
    *   WR DM08540.U 04
    */
    printf("%s %d %s\n", __func__, __LINE__, buf);

    if (send(sockfd, buf, sizeof(buf), 0) < 0) {
        printf("%s %d send error sockfd=%d send_str=%s\n", __func__, __LINE__, (int)sockfd, buf);
        perror("send");
        return 0;
    }
    /*
    *  PLCから WRSコマンドの返信を受け取る
    *  +00001 +00002 +00003 +00004
    */
    recv(sockfd, receive_str, sizeof(receive_str), 0);
    printf("%s %d %s\n", __func__, __LINE__, receive_str);

    // ソケットクローズ
    closesocket(sockfd);
    //printf("%s %d\n", __func__, __LINE__);

    return 0;
}