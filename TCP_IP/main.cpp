#include <stdio.h>
#include <iostream>
#include <winsock2.h>

#include "main.h"

extern int server(char* strs);
extern int client(char* strs);

	
ARGV_STRUCT_st argV;


#pragma comment(lib, "ws2_32.lib")

int main(int argc, char** argv)

{
	// *ctx = NULL;
	char IPaddr[] = "192.168.1.140";

	WORD    wVersionRequested;
	WSADATA wsaData; //wVersion,wHighVersion
	//std::cout << "Hello World! %\n";

	// Windows の場合
	wVersionRequested = MAKEWORD(2, 2);
	int err = WSAStartup(wVersionRequested, &wsaData);
	if (err != 0)
	{
		printf("%s %d argc=%d\n",__func__,__LINE__, argc);
		// Windows での終了設定
		WSACleanup();
		return 1;
	}

	if (argc == 1)
	{
		char str[256] = "";
		printf("keyin cmd\n");
		// %s はスペースで読み込みをやめるので %[^\n] で改行以外を全て取り込むようにする。これで スペースも読み込める
		int size = scanf_s("%[^\n]", str, (unsigned int)sizeof(str));

		printf("%s %d size=%d str=%s\n", __func__, __LINE__, size, str);

		if (strncmp(str, "q", 1) == 0)
		{
			return 0;
		}


		/*main の引数を文字列に代入する*/
		memcpy(argV.strS, str, sizeof(str));
		printf("%s %d str=%s argV.strS=%s\n", __func__, __LINE__, str, argV.strS);

	}
	else
	{
		printf("%s %d argc=%d\n", __func__, __LINE__, argc);
		/*main の引数を文字列に代入する*/
		memset(argV.strS, NULL, sizeof(argV.strS));
		for (int i = 1; i < argc; i++) {
			if (i == 1)
			{
				strcpy_s(argV.strS, argv[i]);
			}
			else
			{
				strcat_s(argV.strS, " ");
				strcat_s(argV.strS, argv[i]);
			}
		}
	}

	//printf("%s %d argvStrS=%s\n",__func__,__LINE__,argV.strS);
	argV.strP = strtok_s(argV.strS, " \n\r", &argV.ctx);
	printf("%s %d argvStrS=%s argvStrP=%s\n", __func__, __LINE__, argV.strS,argV.strP);

	///////////////////////////////////////////////////////
	if (argV.strP == NULL)
	{
		//
		printf("%s %d\n", __func__, __LINE__);
		help();
		return 0;
	}

	// data フォルダが無い場合はフォルダを作成する /////////
	char cmd[100];
	sprintf_s(cmd, "if not exist data mkdir data");
	system(cmd);
	///////////////////////////////////////////////////////

	if (strcmp(argV.strP, "server") == 0)
	{
		server(&IPaddr[0]);

	}
	else if (strcmp(argV.strP, "client") == 0)
	{
		client(&IPaddr[0]);
	}
	else if (strcmp(argV.strP, "test") == 0)
	{
		check_HearBeat();
	}
	else if (strcmp(argV.strP, "rd") == 0)
	{
		cmdR(argV);
	}
	else if (strcmp(argV.strP, "wr") == 0)
	{
		cmdW(argV);
	}
	else if (strcmp(argV.strP, "ver") == 0)
	{
		printf("Version=%lf\n",APL_VERSION);
	}
	else
	{
		help();
	}
	// Windows での終了設定
	WSACleanup();
}
/*
* 
*/
void help(void)
{
	printf("TCP_IP <server>\n");
	printf("       <client>\n");
	printf("       <test>\n");
	printf("       <rd> <adr> <num>\n");
	printf("       <wr> <adr> <data>\n");
	printf("       <ver>\n");
}