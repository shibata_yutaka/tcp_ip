

#include <stdio.h>
#include <string.h>
#include <time.h>

#include "main.h"

FILE* fp;


/*
*
*/
FILE* file_open(int seq_no)
{
	char file_name[256];
	time_t now;
	struct tm local;
	errno_t err;

	now = time(NULL); //現在時刻の取得
	err = localtime_s(&local,&now);
	sprintf_s(file_name, "data/plc_%04d%02d%02d_%02d%02d%02d_%06d.txt"
		, local.tm_year + 1900
		, local.tm_mon + 1
		, local.tm_mday
		, local.tm_hour
		, local.tm_min
		, local.tm_sec
		, seq_no
	);

	err = fopen_s(&fp, file_name, "w");
	if(err!=0)
	{
		printf("%s %d file open error %s\n", __func__, __LINE__, file_name);
		return NULL;
	}
	else
	{
		printf("%s %d file open OK %s\n", __func__, __LINE__, file_name);
	}
	return fp;
}

