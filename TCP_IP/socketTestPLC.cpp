//
// socketTestPLC.c
//
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <sys/types.h>

#include <iostream>

#include <winsock2.h>
#include <ws2tcpip.h>

#include "main.h"

//#include <sys/socket.h>
//#include <netinet/in.h>
//#include <arpa/inet.h>
//#include <unistd.h>

#if 1
    // PLC
//char server_ip[] = "192.168.1.151";
//int server_port = 8501;
#else
    // RasPi 仮想サーバ
char server_ip[] = "127.0.0.1"; //"192.168.1.213";
//int server_port = 8501;
#endif
extern char server_ip[];
extern int server_port;
/*
* FILE状態
*/
#if 0
enum
{
    FILE_OPEN = 0,
    FILE_WRITE,
    FILE_CLOSE,
    FILE_STATE_MAX
} _FILE_STATE;
#endif

extern FILE* fp;

/*
*
*/
#if 0
FILE* fp;
FILE* file_open(int seq_no)
{
    char file_name[256];
    time_t now;
    struct tm local;
    FILE* fp;
    errno_t err;

    now = time(NULL); //現在時刻の取得
    err = localtime_s(&local,&now);
    sprintf_s(file_name, "data/plc_%04d%02d%02d_%02d%02d%02d_%06d.txt"
        , local.tm_year + 1900
        , local.tm_mon + 1
        , local.tm_mday
        , local.tm_hour
        , local.tm_min
        , local.tm_sec
        , seq_no
    );

    if ((err = fopen_s(&fp,file_name, "w")) == NULL)
    {
        printf("%s %d file open error %s\n", __func__, __LINE__, file_name);
    }
    else
    {
        printf("%s %d output file = %s\n", __func__, __LINE__, file_name);
    }
    return fp;
}
#endif
void check_HearBeat(void);

//
//
//
//int main()
#if 0
int main(int argc, char** argv)
{
    printf("%s %d\n", __func__, __LINE__);

#if 0
    if (argc == 1)
    {
        printf("please keyin 192.168.1.213 1234\n");
    }
    else if (argc == 2)
    {
        strcpy(server_ip, argv[1]);
        printf("argc=%d argv=%s\n", argc, argv[1]);
    }
    else if (argc == 3)
    {
        strcpy(server_ip, argv[1]);
        server_port = atoi(argv[2]);
    }
#endif	

    check_HearBeat();

    printf("%s %d\n", __func__, __LINE__);
    return 0;
}
#endif

/*
*
*/
void check_HearBeat(void)
{
    printf("%s %d\n", __func__, __LINE__);
    SOCKET sockfd; //int sockfd;
    struct sockaddr_in addr;

    // ソケット生成
    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("%s %d client socket make error\n", __func__, __LINE__);
        perror("socket");
    }
    else
    {
        printf("%s %d socket make OK\n", __func__, __LINE__);
    }

    // 送信先アドレス・ポート番号設定
    addr.sin_family = AF_INET;
    //addr.sin_port = htons( 1234 );
    //addr.sin_port = htons( 8501 );
    addr.sin_port = htons(server_port);
    //addr.sin_addr.s_addr = inet_addr( "127.0.0.1" );
    // server:rasPi3:192.168.1.213
    // client:rasPi2:192.168.1.212
    //addr.sin_addr.s_addr = inet_addr( "192.168.1.213" );
    //addr.sin_addr.s_addr = inet_addr( "192.168.1.151" );
    inet_pton(AF_INET, server_ip, &addr.sin_addr.s_addr); //addr.sin_addr.s_addr = inet_addr(server_ip);

    printf("%s %d server IP=%s PORT=%d\n", __func__, __LINE__, server_ip, server_port);
    // サーバ接続
    connect(sockfd, (struct sockaddr*)&addr, sizeof(struct sockaddr_in));
    printf("%s %d connect\n", __func__, __LINE__);

    // データ送信
    char send_str[1024];
    char receive_str[1024];



    /*
    * $ gcc socketTestPLC.c -o socketTestPLC
    * $ ./socketTestPLC
    */

    time_t now;
    struct tm local;

    // DM8540 ウォッチドッグを1バイトを連続して読み出す
    int adr = 8540; //ウォッチドッグアドレス
    int num = 4;
    char cHartBeat[4][6];
    int  iHartBeat[4];
    int  oldHartBeat[2][4] = { {-1,-1,-1,-1},{-2,-2,-2,-2} };
    memset(&cHartBeat[0][0], 0x00, 24); //バッファクリア

    int startSw = 0;

    /* ファイル関係 */
    //time_t now;
    //struct tm *local;
    time_t start_time = time(NULL);

    int file_state = FILE_OPEN;
    int file_seq_no = 1;
    int diff_time[2] = { 0,-1 };
    /* */
    //printf("%s %d\n", __func__, __LINE__);
    char str[1024];

    while (1)
    {
        int i = 0;
        sprintf_s(send_str, "RDS DM%05d.U %d\r", adr, num);
        //printf( "send:%s\n",i,send_str);
        if (send(sockfd, send_str, sizeof(send_str), 0) < 0) {
            printf("send error sockfd=%d send_str=%s\n", (int)sockfd, send_str);
            perror("send");
            return;
        }
        else
        {
            //printf("%s %d\n", __func__, __LINE__);
            memset(receive_str, NULL,sizeof(receive_str));
            recv(sockfd, receive_str, sizeof(receive_str), 0);
            //printf("%s %d rcv=%s\n", __func__, __LINE__, receive_str);
            //printf("receive_str=%s\n",receive_str);
            // ハートビートの切り出し
            for (i = 0; i < 4; i++)
            {
                strncpy_s(&cHartBeat[i][0],sizeof(&cHartBeat[i][0]), &receive_str[i * 6], 5);
                iHartBeat[i] = atoi(&cHartBeat[i][0]);
            }
            // 時間表示
            now = time(NULL); //現在時刻の取得
            errno_t err = localtime_s(&local,&now);
            memset(str, 0x0, sizeof(str));
            // 2019/12/27 21:12:34 
            sprintf_s(str, "%04d/%02d/%02d %02d:%02d:%2d "
                , local.tm_year + 1900
                , local.tm_mon + 1
                , local.tm_mday
                , local.tm_hour
                , local.tm_min
                , local.tm_sec);
            //printf( "RDS DM%05d:%s",adr,receive_str );
            //printf("BMS HartBeat ");

            int errFlag = 0;
            int cntMax = 4;  //1; //=4;
            
            for (i = 0; i < cntMax; i++)
            {
                if ((iHartBeat[i] == oldHartBeat[0][i]) && (oldHartBeat[0][i] == oldHartBeat[1][i]))
                {
                    errFlag = -1;
                    sprintf_s(&str[20 + i * 4],sizeof("%02dE "), "%02dE ", iHartBeat[i]);
                    //printf("%s %d str=%s\n",__func__,__LINE__,str);
                }
                else
                {
                    sprintf_s(&str[20 + i * 4],sizeof("%02d  "), "%02d  ", iHartBeat[i]);
                    //printf("%s %d str=%s\n", __func__, __LINE__,str);
                }
                oldHartBeat[1][i] = oldHartBeat[0][i];
                oldHartBeat[0][i] = iHartBeat[i];
            }
            if (errFlag == 0)
            {
                sprintf_s(&str[20 + 4 * 4], sizeof("\n"), "\n");
                //printf("%s %d str=%s\n", __func__, __LINE__,str);
            }
            else
            {
                sprintf_s(&str[20 + 4 * 4], sizeof("-- > Error\n"), " --> Error\n");
                //printf("%s %d str=%s\n", __func__, __LINE__,str);
            }
        }

        /*
        * ファイル関係 開始
        */
        diff_time[0] = (int)difftime(time(NULL), start_time);
        if (diff_time[0] == 0)
        {
            // 60秒*60分=1時間経過で 余り 0 になる
        }
        //if ((diff_time % (60*60))==0)
        else if ((diff_time[0] % (60 * 60)) == 0)
        {
            //printf("%s %d file_state=%d diff_time[0]=%d [1]=%d\n",__func__,__LINE__,file_state,diff_time[0],diff_time[1]);
            if (diff_time[0] == diff_time[1])
            {
            }
            else
            {
                diff_time[1] = diff_time[0];
                if (file_state == FILE_WRITE)
                {
                    file_state = FILE_CLOSE;
                }
            }

        }
        if (file_state == FILE_CLOSE)
        {
            fclose(fp);
            file_state = FILE_OPEN;
        }
        if (file_state == FILE_OPEN)
        {
            fp = file_open(file_seq_no);
            if (fp == NULL)
            {
                return;
            }
            file_seq_no++;
            file_state = FILE_WRITE;
        }
        printf("%s %d str=%s",__func__,__LINE__, str);

        fprintf(fp, "%s", str);
        fflush(fp);


        /*
        * ファイル関係 終わり
        */



        Sleep(1000);
    }





    // ソケットクローズ
    closesocket(sockfd);
    fclose(fp);
    printf("%s %d\n", __func__, __LINE__);

}
